//
//  BAVCommon.h
//  BAVCommon
//
//  Created by Luis Cruz on 27/07/18.
//  Copyright © 2018 Luis Cruz. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for BAVCommon.
FOUNDATION_EXPORT double BAVCommonVersionNumber;

//! Project version string for BAVCommon.
FOUNDATION_EXPORT const unsigned char BAVCommonVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <BAVCommon/PublicHeader.h>


