//
//  BAVDomain.h
//  BAVDomain
//
//  Created by Luis Cruz on 27/07/18.
//  Copyright © 2018 Luis Cruz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <BAVCommon/BAVCommon.h>

//! Project version number for BAVDomain.
FOUNDATION_EXPORT double BAVDomainVersionNumber;

//! Project version string for BAVDomain.
FOUNDATION_EXPORT const unsigned char BAVDomainVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <BAVDomain/PublicHeader.h>
